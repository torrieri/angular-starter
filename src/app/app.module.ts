import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ServiceWorkerModule } from '@angular/service-worker';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { environment } from '../environments/environment';
import { NG_ENTITY_SERVICE_CONFIG } from '@datorama/akita-ng-entity-service';
import { AkitaNgDevtools } from '@datorama/akita-ngdevtools';
import { AkitaNgRouterStoreModule } from '@datorama/akita-ng-router-store';

import { ConfigModule } from '@ngx-config/core';
import { configServiceProvider } from './core/config.service.provider';
import { MetaModule } from '@ngx-meta/core';
import { metaServiceProvider } from './core/meta.service.provider';
import { AuthModule } from '@ngx-auth/core';
import { authServiceProvider } from './core/auth.service.provider';

import { ReactiveFormsModule } from '@angular/forms';
import { AngularMaterialModule } from './angular-material.module';
import { SharedModule } from './shared/shared.module';

import { AppLayoutComponent } from './layouts/app/app-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
import { AppHeaderComponent } from './layouts/app/header/header.component';
import { AppSidebarComponent } from './layouts/app/sidebar/sidebar.component';

import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    AppLayoutComponent,
    AuthLayoutComponent,
    AppHeaderComponent,
    AppSidebarComponent,
    PageNotFoundComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FlexLayoutModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ConfigModule.forRoot(configServiceProvider),
    MetaModule.forRoot(metaServiceProvider),
    AuthModule.forRoot(authServiceProvider),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    environment.production ? [] : AkitaNgDevtools.forRoot(),
    AkitaNgRouterStoreModule.forRoot(),
    ReactiveFormsModule,
    AngularMaterialModule,
    SharedModule,
  ],
  providers: [{ provide: NG_ENTITY_SERVICE_CONFIG, useValue: { baseUrl: 'https://jsonplaceholder.typicode.com' }}],
  bootstrap: [ AppComponent ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
})
export class AppModule { }
