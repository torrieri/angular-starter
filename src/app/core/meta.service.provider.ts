import { MetaLoader, MetaStaticLoader } from '@ngx-meta/core';
import { ConfigService } from '@ngx-config/core';

function metaServiceFactory(config: ConfigService): MetaLoader {
  return new MetaStaticLoader({
    pageTitlePositioning: config.getSettings('seo.pageTitlePositioning'),
    pageTitleSeparator: config.getSettings('seo.pageTitleSeparator'),
    applicationName: config.getSettings('system.applicationName'),
    applicationUrl: config.getSettings('system.applicationUrl'),
    defaults: {
      title: config.getSettings('seo.defaultPageTitle'),
      description: config.getSettings('seo.defaultMetaDescription'),
      generator: 'ng-seed',
      'og:site_name': config.getSettings('system.applicationName'),
      'og:type': 'website',
      'og:locale': config.getSettings('i18n.defaultLanguage.culture'),
      'og:locale:alternate': config.getSettings('i18n.availableLanguages')
        .map((language: any) => language.culture).toString()
    }
  });
}


export const metaServiceProvider = {
  provide: MetaLoader,
  useFactory: metaServiceFactory,
  deps: [ConfigService]
};
