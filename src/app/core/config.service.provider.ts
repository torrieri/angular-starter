import { HttpClient } from '@angular/common/http';
import { ConfigLoader } from '@ngx-config/core';
import { ConfigHttpLoader } from '@ngx-config/http-loader';

function configServiceFactory(http: HttpClient): ConfigLoader {
  return new ConfigHttpLoader(http, './assets/config.local.json');
}

export const configServiceProvider = {
  provide: ConfigLoader,
  useFactory: configServiceFactory,
  deps: [HttpClient]
};