import { AuthLoader, AuthStaticLoader } from '@ngx-auth/core';

function authServiceFactory(): AuthLoader {
  return new AuthStaticLoader({
    backend: {
      endpoint: '/api/auth',
      params: []
    },
    storage: localStorage,
    storageKey: 'currentUser',
    loginRoute: ['/auth/login'],
    defaultUrl: ''
  });
}


export const authServiceProvider = {
  provide: AuthLoader,
  useFactory: authServiceFactory
};


