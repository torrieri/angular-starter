import { async, ComponentFixture, TestBed, tick, fakeAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { AuthModule, AuthService } from '@ngx-auth/core';
import { RouterTestingModule } from '@angular/router/testing';
import { AngularMaterialModule } from '../angular-material.module';
import { HttpClient } from '@angular/common/http';
import { authServiceProvider } from '../core/auth.service.provider';

import { LoginComponent } from './login.component';
import { Router } from '@angular/router';
import { of } from 'rxjs';

const blankUser = { username: '', password: '' };
const validUser = { username: 'admin', password: 'admin' };

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;

  const routerSpy = jasmine.createSpyObj('Router', ['navigateByUrl']);
  const authServiceSpy = jasmine.createSpyObj('AuthService', ['authenticate']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      imports: [
        NoopAnimationsModule,
        ReactiveFormsModule,
        AngularMaterialModule,
        RouterTestingModule,
        HttpClientTestingModule,
        AuthModule.forRoot(authServiceProvider),
      ],
      providers: [
        FormBuilder,
        { provide: Router, useValue: routerSpy },
        { provide: AuthService, useValue: authServiceSpy},
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
    authServiceSpy.authenticate.calls.reset();
    routerSpy.navigateByUrl.calls.reset();
  });

  function updateForm(username: string, password: string): void {
    component.form.controls.username.setValue(username);
    component.form.controls.password.setValue(password);
  }

  function mockRequest(expectUrl: string, method: string, response: any): void {
    const req = httpTestingController.expectOne('/api/auth');
    expect(req.request.method).toEqual('POST');
    req.flush(response);
    httpTestingController.verify();
  }

  function advance(f: ComponentFixture<any>): void {
    tick();
    f.detectChanges();
  }

  describe('Isolated Test', () => {
    it('Component successfully created', () => {
      expect(component).toBeTruthy();
    });

    it('component initial state', () => {
      expect(component.form).toBeDefined();
      expect(component.form.invalid).toBeTruthy();
      expect(component.loginInvalid).toBeFalsy();
    });

    it('form should be valid when valid user', () => {
      updateForm(validUser.username, validUser.password);
      expect(component.form.valid).toBeTruthy();
      expect(component.form.errors).toBeNull();
    });

    it('form value should update from when u change the input', (() => {
      updateForm(validUser.username, validUser.password);
      expect(component.form.value).toEqual(validUser);
    }));

    it('Form invalid should be true when form is invalid', (() => {
      updateForm(blankUser.username, blankUser.password);
      expect(component.form.invalid).toBeTruthy();
    }));
  });

  describe('Shallow Test', () => {
    it('created a form with username and password input and login button', () => {
      const usernameContainer = fixture.debugElement.nativeElement.querySelector('#username');
      const passwordContainer = fixture.debugElement.nativeElement.querySelector('#password');
      const loginBtnContainer = fixture.debugElement.nativeElement.querySelector('#submit');
      expect(usernameContainer).toBeDefined();
      expect(passwordContainer).toBeDefined();
      expect(loginBtnContainer).toBeDefined();
    });

    it('Display Username Error Msg when Username is blank', () => {
      updateForm(blankUser.username, validUser.password);
      fixture.detectChanges();

      const button = fixture.debugElement.nativeElement.querySelector('button');
      button.click();
      fixture.detectChanges();

      const usernameErrorMsg = fixture.debugElement.nativeElement.querySelector('#username-error');
      expect(usernameErrorMsg).toBeDefined();
      expect(usernameErrorMsg.innerHTML).toContain('Please provide a valid username');
    });

    it('Display Password Error Msg when Password is blank', () => {
      updateForm(validUser.username, blankUser.password);
      fixture.detectChanges();

      const button = fixture.debugElement.nativeElement.querySelector('button');
      button.click();
      fixture.detectChanges();

      const passwordErrorMsg = fixture.debugElement.nativeElement.querySelector('#password-error');
      expect(passwordErrorMsg).toBeDefined();
      expect(passwordErrorMsg.innerHTML).toContain('Please provide a valid password');
    });

    it('Display Both Username & Password Error Msg when both field is blank', () => {
      updateForm(blankUser.username, blankUser.password);
      fixture.detectChanges();

      const button = fixture.debugElement.nativeElement.querySelector('button');
      button.click();
      fixture.detectChanges();

      const usernameErrorMsg = fixture.debugElement.nativeElement.querySelector('#username-error');
      const passwordErrorMsg = fixture.debugElement.nativeElement.querySelector('#password-error');

      expect(usernameErrorMsg).toBeDefined();
      expect(usernameErrorMsg.innerHTML).toContain('Please provide a valid username');

      expect(passwordErrorMsg).toBeDefined();
      expect(passwordErrorMsg.innerHTML).toContain('Please provide a valid password');
    });

    it('When username is blank, username field should display red outline ', () => {
      updateForm(blankUser.username, validUser.password);
      fixture.detectChanges();
      const button = fixture.debugElement.nativeElement.querySelector('button');
      button.click();
      fixture.detectChanges();

      const inputs = fixture.debugElement.nativeElement.querySelectorAll('input');
      const usernameInput = inputs[0];

      expect(usernameInput.classList).toContain('ng-invalid');
    });

    it('When password is blank, password field should display red outline ', () => {
      updateForm(validUser.username, blankUser.password);
      fixture.detectChanges();
      const button = fixture.debugElement.nativeElement.querySelector('button');
      button.click();
      fixture.detectChanges();

      const inputs = fixture.debugElement.nativeElement.querySelectorAll('input');
      const passwordInput = inputs[1];

      expect(passwordInput.classList).toContain('ng-invalid');
    });
  });

  describe('Integrated Test', () => {
    it('should compile', () => {
      expect(component).toBeTruthy();
    });

    it('should not redirect an invalid authentication', fakeAsync(() => {
      updateForm(validUser.username, 'fake');

      authServiceSpy.authenticate.and.returnValue(of(false));
      authServiceSpy.isAuthenticated = false;

      const button = fixture.debugElement.nativeElement.querySelector('button');
      button.click();
      advance(fixture);

      expect(authServiceSpy.authenticate.calls.count()).toBe(1);
      expect(routerSpy.navigateByUrl).not.toHaveBeenCalled();
    }));

    it('should redirect a valid authentication', fakeAsync(() => {
      updateForm(validUser.username, validUser.password);

      authServiceSpy.authenticate.and.returnValue(of(true));
      authServiceSpy.isAuthenticated = true;

      const button = fixture.debugElement.nativeElement.querySelector('button');
      button.click();
      advance(fixture);

      expect(authServiceSpy.authenticate).toHaveBeenCalled();
      expect(routerSpy.navigateByUrl).toHaveBeenCalled();
      expect(routerSpy.navigateByUrl.calls.first().args[0]).toBe('/dashboard');
    }));
  });
});
