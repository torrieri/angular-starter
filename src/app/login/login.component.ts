import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AuthService } from '@ngx-auth/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.styl'],
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  public loginInvalid: boolean;

  constructor(private router: Router, private fb: FormBuilder, private readonly auth: AuthService) {}

  ngOnInit(): void {
    this.loginInvalid = false;
    this.form = this.fb.group({
      username: [null, Validators.required],
      password: [null, Validators.required],
    });
  }

  onSubmit(): void {
    if (this.form.valid) {
      this.login();
    }
  }

  login(): void {
    const username: string = this.form.get('username').value;
    const password: string = this.form.get('password').value;

    this.auth.authenticate(username, password)
      .subscribe(() => {
        if (!this.auth.isAuthenticated) {
          this.loginInvalid = true;
        } else {
          this.router.navigateByUrl('/dashboard');
        }
      });
  }

  logout(): void {
    this.auth.invalidate();
  }
}
