import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '@ngx-auth/core';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { LoginComponent } from './login/login.component';
import { AppLayoutComponent } from './layouts/app/app-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';

const routes: Routes = [
  { 
    path: '',
    component: AppLayoutComponent,
    children: [
      {
        path: '',
        redirectTo: '/dashboard',
        pathMatch: 'full',
      },
      {
        path: 'dashboard',
        loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule),
        // canActivate: [AuthGuard],
      },
      // {
      //   path: 'account',
      //   children: [
      //     {
      //       path: 'profile',
      //       component: ProfileComponent,
      //     },
      //     {
      //       path: 'change-password',
      //       component: ChangePasswordComponent,
      //     }
      //   ],
      //   canActivateChild: [AuthGuard],
      // },
    ]
  },
  {
    path: 'auth',
    component: AuthLayoutComponent,
    children: [
      {
        path: 'login',
        component: LoginComponent,
      },
      { path: '**', component: PageNotFoundComponent }
    ],
  },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
